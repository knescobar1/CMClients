package com.banquito.clients.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class ApplicationValues {
  private final String mongoHost;
  private final String mongoDB;
  private final String mongoUsr;
  private final String mongoPwd;
  private final String mongoAut;

  public ApplicationValues(
      @Value("${banquito.clients.mongo.host}") String mongoHost,
      @Value("${banquito.clients.mongo.db}") String mongoDB,
      @Value("${banquito.clients.mongo.usr}") String mongoUsr,
      @Value("${banquito.clients.mongo.pwd}") String mongoPwd,
      @Value("${banquito.clients.mongo.aut}") String mongoAut) {

    this.mongoHost = mongoHost;
    this.mongoDB = mongoDB;
    this.mongoUsr = mongoUsr;
    this.mongoPwd = mongoPwd;
    this.mongoAut = mongoAut;
  }
}
