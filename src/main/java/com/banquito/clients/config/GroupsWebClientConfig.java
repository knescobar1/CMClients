package com.banquito.clients.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Component
public class GroupsWebClientConfig {
  @Getter private final WebClient webClient;

  public GroupsWebClientConfig(@Value("${banquito.core.customers.base-url}") String baseUrl) {
    this.webClient = WebClient.create(baseUrl + "/groups");
  }
}
