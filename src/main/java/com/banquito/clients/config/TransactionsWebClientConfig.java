package com.banquito.clients.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class TransactionsWebClientConfig {
  @Getter private final WebClient transactionsWebClient;

  public TransactionsWebClientConfig(
      @Value("${banquito.core.accounts.base-url}") String coreAccountsBaseURL) {
    this.transactionsWebClient = WebClient.create(coreAccountsBaseURL + "/transaction");
  }
}
