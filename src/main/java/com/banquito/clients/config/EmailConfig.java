package com.banquito.clients.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.SimpleMailMessage;

@Configuration
public class EmailConfig {
  @Getter private final String emailAddress;

  public EmailConfig(@Value("${spring.mail.username}") String emailAddress) {
    this.emailAddress = emailAddress;
  }

  @Bean
  public SimpleMailMessage userAndPasswordTemplateMailBody() {
    String messageBody =
        "<h2 style='text-align: center'>Se ha registrado satisfactoriamente</h2></br>"
            + "<p>Ingrese con las siguientes credenciales: </p></br>"
            + "<b>Usuario:&nbsp;</b>"
            + "%s"
            + "</br>"
            + "<b>Contraseña:&nbsp;</b>"
            + "%s";

    SimpleMailMessage mailMessage = new SimpleMailMessage();
    mailMessage.setText(messageBody);

    return mailMessage;
  }
}
