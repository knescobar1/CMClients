package com.banquito.clients.dao;

import com.banquito.clients.model.Account;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

public interface AccountRepository extends MongoRepository<Account, String> {
  Optional<Account> findByGroupInternalIdAndAccountTypeAndAccountNumber(
      String groupId, String accountType, BigInteger accountNumber);

  List<Account> findByGroupInternalId(String groupInternalId);

  Optional<Account> findByAccountNumber(BigInteger accountNumber);

  Optional<Account> findByAccountNumberAndServiceType(BigInteger accountNumber, String serviceType);

  Optional<Account> findByGroupInternalIdAndServiceType(
      String groupInternalIdr, String serviceType);

  List<Account> findByGroupInternalIdIn(List<String> groupInternalIds);
}
