package com.banquito.clients.dao;

import com.banquito.clients.model.Company;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface CompanyRepository extends MongoRepository<Company, String> {
  Optional<Company> findByCompanyRuc(String companyRuc);

  Optional<Company> findByCompanyName(String companyName);

  Optional<Company> findByGroupInternalId(String internalId);
}
