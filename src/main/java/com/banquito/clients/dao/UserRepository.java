package com.banquito.clients.dao;

import com.banquito.clients.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends MongoRepository<User, String> {

  Optional<User> findByMail(String mail);

  List<User> findByGroupInternalId(String groupInternalId);

  Optional<User> findByUsername(String username);

  Optional<User> findByMailOrUsername(String mail, String username);

  List<User> findByGroupInternalIdAndState(String groupInternalId, String state);

  List<User> findByGroupInternalIdAndRole(String groupInternalId, String role);
}
