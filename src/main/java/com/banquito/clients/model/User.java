package com.banquito.clients.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document(collection = "users")
@TypeAlias("users")
public class User {

  @Id private String id;

  @Indexed(name = "idxu_users_username", unique = true)
  private String username;

  private String password;

  private String name;

  @Indexed(name = "idxu_users_email", unique = true)
  private String mail;

  private String role;

  @Indexed(name = "idx_users_state", unique = false)
  private String state;

  private String groupInternalId;
}
