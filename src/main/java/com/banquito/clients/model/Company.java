package com.banquito.clients.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document(collection = "companies")
@TypeAlias("companies")
public class Company {

  @Id private String id;

  @Indexed(name = "idxu_companies_companyRuc", unique = true)
  private String companyRuc;

  private String companyName;

  private String phoneReference;

  private String businessName;

  private String groupInternalId;
}
