package com.banquito.clients.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.math.BigInteger;

@Data
@Builder
@Document(collection = "accounts")
@TypeAlias("accounts")
public class Account {

  @Id private String id;

  @Indexed(name = "idxu_accounts_accountNumber", unique = true)
  private BigInteger accountNumber;

  private String accountType;

  private String serviceType;

  private BigDecimal accountingBalance;

  private BigDecimal availableBalance;

  private String accountState;

  private String groupInternalId;
}
