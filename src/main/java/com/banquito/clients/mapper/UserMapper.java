package com.banquito.clients.mapper;

import com.banquito.clients.dto.UserDTO;
import com.banquito.clients.model.User;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserMapper {

  public static User buildUser(UserDTO dto) {
    return User.builder()
        .username(dto.getUsername())
        .password(dto.getPassword())
        .name(dto.getName())
        .mail(dto.getMail())
        .role(dto.getRole())
        .state(dto.getState())
        .groupInternalId(dto.getGroupInternalId())
        .build();
  }

  public static UserDTO buildUserDTO(User user) {
    return UserDTO.builder()
        .username(user.getUsername())
        .name(user.getName())
        .password(user.getPassword())
        .mail(user.getMail())
        .role(user.getRole())
        .state(user.getState())
        .groupInternalId(user.getGroupInternalId())
        .build();
  }
}
