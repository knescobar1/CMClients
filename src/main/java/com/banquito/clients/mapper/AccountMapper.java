package com.banquito.clients.mapper;

import com.banquito.clients.dto.AccountCoreDTO;
import com.banquito.clients.dto.AccountDTO;
import com.banquito.clients.model.Account;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AccountMapper {

  public static Account buildAccount(AccountCoreDTO account) {
    return Account.builder()
        .accountNumber(account.getAccountNumber())
        .accountType(account.getAccountType())
        .accountingBalance(account.getBalance())
        .availableBalance(account.getBalance())
        .accountState(account.getState())
        .groupInternalId(account.getCustomerGroupId())
        .build();
  }

  public static AccountDTO buildAccountDTO(Account account) {
    return AccountDTO.builder()
        .accountNumber(account.getAccountNumber())
        .accountType(account.getAccountType())
        .serviceType(account.getServiceType())
        .accountingBalance(account.getAccountingBalance())
        .availableBalance(account.getAvailableBalance())
        .accountState(account.getAccountState())
        .groupInternalId(account.getGroupInternalId())
        .build();
  }
}
