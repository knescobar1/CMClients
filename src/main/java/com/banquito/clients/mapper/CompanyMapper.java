package com.banquito.clients.mapper;

import com.banquito.clients.dto.CompanyDTO;
import com.banquito.clients.model.Company;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CompanyMapper {

  public static Company buildCompany(CompanyDTO companyDto) {
    return Company.builder()
        .companyRuc(companyDto.getCompanyRuc())
        .companyName(companyDto.getCompanyName())
        .phoneReference(companyDto.getPhoneReference())
        .businessName(companyDto.getBusinessName())
        .groupInternalId(companyDto.getGroupInternalId())
        .build();
  }

  public static CompanyDTO buildCompanyDTO(Company company) {
    return CompanyDTO.builder()
        .companyRuc(company.getCompanyRuc())
        .companyName(company.getCompanyName())
        .phoneReference(company.getPhoneReference())
        .businessName(company.getBusinessName())
        .groupInternalId(company.getGroupInternalId())
        .build();
  }
}
