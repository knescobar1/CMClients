package com.banquito.clients.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class InactiveAccountException extends RuntimeException {
  public InactiveAccountException(String message) {
    super(message);
  }
}
