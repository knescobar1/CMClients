package com.banquito.clients.service;

import com.banquito.clients.config.TransactionsWebClientConfig;
import com.banquito.clients.dao.AccountRepository;
import com.banquito.clients.dto.AccountDTO;
import com.banquito.clients.dto.TransactionDTO;
import com.banquito.clients.enums.AccountStateEnum;
import com.banquito.clients.enums.ServiceTypeEnum;
import com.banquito.clients.exception.AccountAssignedException;
import com.banquito.clients.exception.CreateException;
import com.banquito.clients.exception.InactiveAccountException;
import com.banquito.clients.exception.NotFoundException;
import com.banquito.clients.mapper.AccountMapper;
import com.banquito.clients.model.Account;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class AccountService {

  private final TransactionsWebClientConfig transactionsWebClient;

  private final AccountRepository accountRepository;

  public Account registerAccount(Account account) {
    Optional<Account> accountOpt =
        accountRepository.findByGroupInternalIdAndServiceType(
            account.getGroupInternalId(), ServiceTypeEnum.PAYMENTS_AND_COLLECTIONS.getValue());
    if (accountOpt.isPresent()) {
      throw new AccountAssignedException("Error, la cuenta ya ha sido asignada");
    }
    accountIsPresentByAccountNumberAndServiceType(
        account.getAccountNumber(), account.getServiceType());
    accountIsPresentByGroupInternalIdAndServiceType(
        account.getGroupInternalId(), account.getServiceType());
    return this.accountRepository.save(account);
  }

  public List<TransactionDTO> obtainAccountTransactionDetails(BigInteger accountNumber) {
    Account account = this.findByAccountNumber(accountNumber);

    return Stream.of(this.fetchTransactionsByAccountNumber(accountNumber))
        .peek(transactionDTO -> transactionDTO.setServiceLevel(account.getServiceType()))
        .collect(Collectors.toList());
  }

  private TransactionDTO[] fetchTransactionsByAccountNumber(BigInteger accountNumber) {
    try {
      TransactionDTO[] transactions =
          this.transactionsWebClient
              .getTransactionsWebClient()
              .get()
              .uri("/" + accountNumber)
              .retrieve()
              .bodyToMono(TransactionDTO[].class)
              .block();

      if (transactions == null) {
        throw new NotFoundException(
            "No se ha encontrado movimientos para la cuenta " + accountNumber);
      }

      return transactions;
    } catch (Exception e) {
      e.printStackTrace();
      throw new NotFoundException(
          "No se ha encontrado movimientos para la cuenta " + accountNumber);
    }
  }

  public AccountDTO modifyAccount(AccountDTO dto) {
    Optional<Account> accountOpt =
        accountRepository.findByGroupInternalIdAndServiceType(
            dto.getGroupInternalId(), ServiceTypeEnum.PAYMENTS_AND_COLLECTIONS.getValue());
    if (accountOpt.isPresent()) {
      throw new AccountAssignedException("Error, la cuenta ya ha sido asignada");
    }
    Account accountDB = this.findByAccountNumber(dto.getAccountNumber());
    accountIsPresentByAccountNumberAndServiceType(
        accountDB.getAccountNumber(), dto.getServiceType());
    accountIsPresentByGroupInternalIdAndServiceType(
        accountDB.getGroupInternalId(), dto.getServiceType());
    accountDB.setServiceType(dto.getServiceType());
    this.accountRepository.save(accountDB);
    return AccountMapper.buildAccountDTO(accountDB);
  }

  public AccountDTO modifyServiceAccount(AccountDTO dto) {
    Account accountDB = this.findByAccountNumber(dto.getAccountNumber());
    accountDB.setServiceType(dto.getServiceType());
    this.accountRepository.save(accountDB);
    return AccountMapper.buildAccountDTO(accountDB);
  }

  public AccountDTO updateAvailableBalance(AccountDTO dto) {
    Account accountDB = this.findByAccountNumber(dto.getAccountNumber());
    accountDB.setAvailableBalance(dto.getAvailableBalance());
    this.accountRepository.save(accountDB);
    return AccountMapper.buildAccountDTO(accountDB);
  }

  public Account findByAccountNumber(BigInteger accountNumber) throws NotFoundException {

    Account account =
        this.accountRepository
            .findByAccountNumber(accountNumber)
            .orElseThrow(() -> new NotFoundException("La cuenta indicada no existe"));

    boolean isAccountInactive =
        AccountStateEnum.INACTIVE.getValue().equals(account.getAccountState());
    if (isAccountInactive) {
      throw new InactiveAccountException("La cuenta con nro. " + accountNumber + " está inactiva");
    }

    return account;
  }

  private void accountIsPresentByAccountNumberAndServiceType(
      BigInteger accountNumber, String serviceType) throws CreateException {
    Optional<Account> accountOpt =
        accountRepository.findByAccountNumberAndServiceType(accountNumber, serviceType);
    if (accountOpt.isPresent()) {
      throw new AccountAssignedException("Error, la cuenta ya ha sido asignada");
    }
  }

  private void accountIsPresentByGroupInternalIdAndServiceType(
      String groupInternalId, String serviceType) throws CreateException {
    Optional<Account> accountOpt =
        accountRepository.findByGroupInternalIdAndServiceType(groupInternalId, serviceType);
    if (accountOpt.isPresent()) {
      throw new AccountAssignedException("Error, el servicio ya ha sido asignado");
    }
  }

  public List<Account> obtainByGroupInternalId(String groupInternalId) {
    return this.accountRepository.findByGroupInternalId(groupInternalId);
  }

  public List<Account> obtainByGroupInternalIdAndServiceType(
      String groupInternalId, String serviceType) {
    List<Account> accounts = new ArrayList<>();
    Optional<Account> accountOpt =
        accountRepository.findByGroupInternalIdAndServiceType(groupInternalId, serviceType);
    if (accountOpt.isPresent()) {
      accounts.add(accountOpt.get());
    } else {
      throw new NotFoundException("No se encontraron cuentas para el grupo indicado");
    }
    return accounts;
  }
}
