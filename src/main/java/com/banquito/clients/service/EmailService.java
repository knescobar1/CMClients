package com.banquito.clients.service;

import com.banquito.clients.config.EmailConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class EmailService {
  private final JavaMailSender emailSender;
  private final SimpleMailMessage template;
  private final EmailConfig emailConfig;

  @Async
  public void sendUserAndPasswordEmail(String userEmail, String username, String password)
      throws MessagingException {
    MimeMessage mimeMessage = emailSender.createMimeMessage();
    MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "utf-8");

    String emailBody =
        String.format(Objects.requireNonNull(template.getText()), username, password);

    message.setFrom(this.emailConfig.getEmailAddress());
    message.setTo(userEmail);
    message.setSubject("Bienvenido al sistema de Cash Management");
    message.setText(emailBody, true);

    emailSender.send(mimeMessage);
  }
}
