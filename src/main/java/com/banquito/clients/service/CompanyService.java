package com.banquito.clients.service;

import com.banquito.clients.config.GroupsWebClientConfig;
import com.banquito.clients.dao.CompanyRepository;
import com.banquito.clients.dao.UserRepository;
import com.banquito.clients.dto.GroupDTO;
import com.banquito.clients.enums.RoleEnum;
import com.banquito.clients.enums.UserStateEnum;
import com.banquito.clients.exception.CreateException;
import com.banquito.clients.exception.EmailException;
import com.banquito.clients.exception.NotFoundException;
import com.banquito.clients.model.Company;
import com.banquito.clients.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class CompanyService {
  private final GroupsWebClientConfig groupsWebClientConfig;
  private final PasswordEncoder passwordEncoder;
  private final EmailService emailService;
  private final UserRepository userRepository;
  private final CompanyRepository companyRepository;

  public Company create(Company company) {

    companyIsPresentByRuc(company.getCompanyRuc());

    GroupDTO group = this.findGroupByInternalId(company.getGroupInternalId());

    company.setCompanyName(group.getName());
    company.setBusinessName(group.getName());

    String username = group.getEmail().split("@", 2)[0];
    String rawPassword = RandomStringUtils.randomAlphabetic(8);

    User newUser =
        User.builder()
            .username(username)
            .password(passwordEncoder.encode(rawPassword))
            .name(group.getName())
            .mail(group.getEmail())
            .role(RoleEnum.REPRESENTANTELEGAL.getValue())
            .state(UserStateEnum.ACTIVE.getValue())
            .groupInternalId(group.getInternalId())
            .build();

    log.info("Creando usuario {} para el grupo con id {}.", newUser, group.getInternalId());
    log.info("Creando empresa {} para el grupo con id {}.", company, group.getInternalId());
    this.userRepository.save(newUser);
    this.companyRepository.save(company);

    try {
      log.info("Enviando correo con las credenciales a {}.", group.getEmail());
      this.emailService.sendUserAndPasswordEmail(group.getEmail(), username, rawPassword);
    } catch (MessagingException e) {
      log.error("Error al enviar el correo a {}.", group.getEmail(), e);
      throw new EmailException("Lo sentimos, no hemos podido enviar el correo.");
    }

    return company;
  }

  private void companyIsPresentByRuc(String companyRuc) throws CreateException {
    Optional<Company> companyOpt = companyRepository.findByCompanyRuc(companyRuc);
    if (companyOpt.isPresent()) {
      throw new CreateException("Error, la empresa indicada ya existe");
    }
  }

  private GroupDTO findGroupByInternalId(String internalId) {
    try {
      GroupDTO group =
          this.groupsWebClientConfig
              .getWebClient()
              .get()
              .uri("/" + internalId)
              .retrieve()
              .bodyToMono(GroupDTO.class)
              .block();

      if (group == null) {
        throw new NotFoundException("No se ha encontrado un grupo con el id " + internalId);
      }

      return group;
    } catch (Exception e) {
      e.printStackTrace();
      throw new NotFoundException("No se ha encontrado un grupo con el id " + internalId);
    }
  }

  public Company findCompanyByInternalId(String id) {
    return this.companyRepository
        .findByGroupInternalId(id)
        .orElseThrow(
            () -> new NotFoundException("No se ha encontrado una empresa con el id " + id));
  }

  public Company findByRUC(String ruc) {
    return this.companyRepository
        .findByCompanyRuc(ruc)
        .orElseThrow(
            () -> new NotFoundException("No se ha encontrado una empresa con el ruc " + ruc));
  }

  public Company findByName(String name) {
    return this.companyRepository
        .findByCompanyName(name)
        .orElseThrow(
            () -> new NotFoundException("No se ha encontrado una empresa con el nombre " + name));
  }
}
