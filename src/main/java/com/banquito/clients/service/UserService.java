package com.banquito.clients.service;

import com.banquito.clients.dao.UserRepository;
import com.banquito.clients.dto.UserDTO;
import com.banquito.clients.enums.UserStateEnum;
import com.banquito.clients.exception.CreateException;
import com.banquito.clients.exception.LoginException;
import com.banquito.clients.exception.NotFoundException;
import com.banquito.clients.mapper.UserMapper;
import com.banquito.clients.model.User;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService{

  private final PasswordEncoder passwordEncoder;
  private final UserRepository userRepository;

  public User create(User user) {
    this.userRepository
        .findByMailOrUsername(user.getMail(), user.getUsername())
        .ifPresent(
            userDB -> {
              boolean isEmailTaken = userDB.getMail().equals(user.getMail());
              boolean isUsernameTaken = userDB.getUsername().equals(user.getUsername());

              if (isEmailTaken && isUsernameTaken) {
                throw new CreateException("Error, usuario y correo no disponibles");
              }

              if (isEmailTaken) throw new CreateException("Error, email no disponible");
              if (isUsernameTaken) throw new CreateException("Error, usuario no disponible");
            });

    String rawPassword = RandomStringUtils.randomAlphabetic(8);

    user.setPassword(rawPassword);
    user.setState(UserStateEnum.ACTIVE.getValue());
    this.userRepository.save(user);
    return user;
  }

  public User loginCM(String username, String password) {
    User user = userIsPresentByUsername(username);
    boolean userIsInactive = user.getState().equals(UserStateEnum.INACTIVE.getValue());

    if (userIsInactive) {
      throw new LoginException("El usuario indicado se encuentra inactivo");
    }

    boolean isCorrectPassword = passwordEncoder.matches(password, user.getPassword());
    if (!isCorrectPassword) {
      throw new LoginException("El usuario o contraseña es incorrecto(s).");
    }

    return user;
  }
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = userRepository.findByUsername(username).get();
    List <SimpleGrantedAuthority> authorities = new ArrayList<>();
    authorities.add((new SimpleGrantedAuthority(user.getRole())));
    return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
  }

  private User userIsPresentByUsername(String username) throws NotFoundException {
    return userRepository
        .findByUsername(username)
        .orElseThrow(() -> new NotFoundException("Error, usuario no existe"));
  }

  public UserDTO updateUser(UserDTO userDto) {
    User userDB =
        this.userRepository
            .findByUsername(userDto.getUsername())
            .orElseThrow(() -> new NotFoundException("Error, usuario no existe"));

    userDB.setMail(userDto.getMail());
    userDB.setName(userDto.getName());
    userDB.setState(userDto.getState());
    userDB.setRole(userDto.getRole());

    return UserMapper.buildUserDTO(this.userRepository.save(userDB));
  }

  public List<User> activeUsersByCompany(String groupInternalId) {
    return this.userRepository.findByGroupInternalIdAndState(
        groupInternalId, UserStateEnum.ACTIVE.getValue());
  }

  public List<User> usersByCompanyAndRole(String groupInternalId, String role) {
    return this.userRepository.findByGroupInternalIdAndRole(groupInternalId, role);
  }

  public User blockUser(User user) {
    User userdb = this.userRepository.findByMail(user.getMail()).get();
    userdb.setState(UserStateEnum.INACTIVE.getValue());
    this.userRepository.save(userdb);
    return userdb;
  }

  public User encodeNewUserPassword(String mail){
    User userdb = this.userRepository.findByMail(mail).get();
    userdb.setPassword(passwordEncoder.encode(userdb.getPassword()));
    this.userRepository.save(userdb);
    return userdb;
  }
}
