package com.banquito.clients.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum UserStateEnum {
  ACTIVE("ACT", "Active"),
  INACTIVE("INA", "Inactive");

  private final String value;
  private final String text;
}
