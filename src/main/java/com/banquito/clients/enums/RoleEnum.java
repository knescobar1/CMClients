package com.banquito.clients.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum RoleEnum {
  INGRESADOR("ING", "Ingresador"),
  PROCESADOR("PRO", "Procesador"),
  REPRESENTANTELEGAL("RPL", "Representante legal"),
  ADMINISTRADOR("ADM", "Administrador");

  private final String value;
  private final String text;
}
