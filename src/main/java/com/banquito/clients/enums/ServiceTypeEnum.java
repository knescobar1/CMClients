package com.banquito.clients.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum ServiceTypeEnum {
  PAYMENTS("PAY", "Payments"),
  COLLECTIONS("COL", "Collections"),
  PAYMENTS_AND_COLLECTIONS("PNC", "PaymentsAndCollections"),
  UNASSIGNED("NAS", "Unassigned");

  private final String value;
  private final String text;
}
