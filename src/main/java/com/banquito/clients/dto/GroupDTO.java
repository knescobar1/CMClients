package com.banquito.clients.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GroupDTO {

  private String groupId;

  private String internalId;

  private String documentType;

  private String documentId;

  private String name;

  private String email;

  private CustomerSegmentDTO segment;

  private String assignedBranch;

  private Date creationDate;
}
