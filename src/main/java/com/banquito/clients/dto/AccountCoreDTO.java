package com.banquito.clients.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

@Data
@Builder
public class AccountCoreDTO {
    private BigInteger accountNumber;

    private String familyId; // Used for creation

    private String customerGroupId; // Used for creation

    private String accountType;

    private Date openingDate;

    private Date maturityDate;

    private BigDecimal balance;

    private String state;
}
