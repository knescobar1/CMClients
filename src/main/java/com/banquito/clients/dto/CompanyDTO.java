package com.banquito.clients.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CompanyDTO {

  private String companyRuc;

  private String companyName;

  private String phoneReference;

  private String businessName;

  private String groupInternalId;
}
