package com.banquito.clients.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;

@Data
@Builder
public class AccountDTO {

  private String id;

  private BigInteger accountNumber;

  private String accountType;

  private String serviceType;

  private BigDecimal accountingBalance;

  private BigDecimal availableBalance;

  private String accountState;

  private String groupInternalId;
}
