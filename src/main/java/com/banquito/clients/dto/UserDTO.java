package com.banquito.clients.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserDTO {

  private String internalId;

  private String username;

  private String password;

  private String name;

  private String mail;

  private String role;

  private String state;

  private String groupInternalId;
}
