package com.banquito.clients;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class CmClientsApplication {

  public static void main(String[] args) {
    SpringApplication.run(CmClientsApplication.class, args);
  }
}
