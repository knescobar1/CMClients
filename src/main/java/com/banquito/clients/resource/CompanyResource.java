package com.banquito.clients.resource;

import com.banquito.clients.dto.CompanyDTO;
import com.banquito.clients.mapper.CompanyMapper;
import com.banquito.clients.model.Company;
import com.banquito.clients.service.CompanyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/company")
@RequiredArgsConstructor
public class CompanyResource {

  private final CompanyService companyService;

  @GetMapping("/id/{id}")
  public ResponseEntity<CompanyDTO> findCompanyByInternalId(@PathVariable String id) {
    Company company = this.companyService.findCompanyByInternalId(id);
    return ResponseEntity.ok(CompanyMapper.buildCompanyDTO(company));
  }

  @GetMapping("/ruc/{ruc}")
  public ResponseEntity<CompanyDTO> findByRUC(@PathVariable String ruc) {
    Company company = this.companyService.findByRUC(ruc);
    return ResponseEntity.ok(CompanyMapper.buildCompanyDTO(company));
  }

  @GetMapping("/name/{name}")
  public ResponseEntity<CompanyDTO> findByName(@PathVariable String name) {
    Company company = this.companyService.findByName(name);
    return ResponseEntity.ok(CompanyMapper.buildCompanyDTO(company));
  }

  @PostMapping
  public ResponseEntity<CompanyDTO> create(@RequestBody CompanyDTO dto) {
    Company company = this.companyService.create(CompanyMapper.buildCompany(dto));
    return ResponseEntity.ok(CompanyMapper.buildCompanyDTO(company));
  }
}
