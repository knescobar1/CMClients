package com.banquito.clients.resource;

import com.banquito.clients.dto.AccountDTO;
import com.banquito.clients.dto.TransactionDTO;
import com.banquito.clients.mapper.AccountMapper;
import com.banquito.clients.model.Account;
import com.banquito.clients.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountResource {
  private final AccountService accountService;

  @GetMapping("/{accountNumber}")
  public ResponseEntity<AccountDTO> findAccountByNumber(@PathVariable BigInteger accountNumber) {
    Account account = this.accountService.findByAccountNumber(accountNumber);
    return ResponseEntity.ok(AccountMapper.buildAccountDTO(account));
  }

  @GetMapping("/{accountNumber}/transactions")
  public ResponseEntity<List<TransactionDTO>> findTransactionsByAccountNumber(
      @PathVariable BigInteger accountNumber) {
    List<TransactionDTO> transactions =
        this.accountService.obtainAccountTransactionDetails(accountNumber);
    return ResponseEntity.ok(transactions);
  }

  @PostMapping
  public ResponseEntity<AccountDTO> registerAccountOnCm(@RequestBody Account account) {
    Account account1 = this.accountService.registerAccount(account);
    return ResponseEntity.ok(AccountMapper.buildAccountDTO(account1));
  }

  @PutMapping
  public ResponseEntity<AccountDTO> modifyAccount(@RequestBody AccountDTO dto) {
    return ResponseEntity.ok(this.accountService.modifyAccount(dto));
  }

  @PutMapping(path = "/service")
  public ResponseEntity<AccountDTO> modifyServiceAccount(@RequestBody AccountDTO dto) {
    return ResponseEntity.ok(this.accountService.modifyServiceAccount(dto));
  }

  @PutMapping(path = "/balance")
  public ResponseEntity<AccountDTO> updateAvailableBalance(@RequestBody AccountDTO dto) {
    try {
      return ResponseEntity.ok(this.accountService.updateAvailableBalance(dto));
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping(path = "/listAccountsByGroupInternalId/{groupInternalId}")
  public ResponseEntity<List<AccountDTO>> listAccountsByGroupInternalId(
      @PathVariable String groupInternalId) {
    return ResponseEntity.ok(
        this.accountService.obtainByGroupInternalId(groupInternalId).stream()
            .map(AccountMapper::buildAccountDTO)
            .collect(Collectors.toList()));
  }

  @GetMapping(path = "/listAccountsByGroupInternalIdAndServiceType/{groupInternalId}/{serviceType}")
  public ResponseEntity<List<AccountDTO>> listAccountsByGroupInternalIdAndServiceType(
      @PathVariable String groupInternalId, @PathVariable String serviceType) {
    return ResponseEntity.ok(
        this.accountService.obtainByGroupInternalIdAndServiceType(groupInternalId, serviceType).stream()
            .map(AccountMapper::buildAccountDTO)
            .collect(Collectors.toList()));
  }
}
