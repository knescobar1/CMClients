package com.banquito.clients.resource;

import com.banquito.clients.dto.LoginDTO;
import com.banquito.clients.dto.UserDTO;
import com.banquito.clients.enums.RoleEnum;
import com.banquito.clients.mapper.UserMapper;
import com.banquito.clients.model.User;
import com.banquito.clients.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/user")
@RequiredArgsConstructor
public class UserResource {
  private final UserService service;

  @GetMapping(path = "/company/{id}")
  public ResponseEntity<List<User>> listUsersByCompany(@PathVariable String id) {
    return ResponseEntity.ok(service.activeUsersByCompany(id));
  }

  @GetMapping(path = "/search/{groupInternalId}/{role}")
  public ResponseEntity<List<User>> listUsersByCompanyAndRole(
      @PathVariable String groupInternalId, @PathVariable String role) {
    return ResponseEntity.ok(service.usersByCompanyAndRole(groupInternalId, role));
  }

  @GetMapping(path = "/rpl/{groupInternalId}")
  public ResponseEntity<User> representativeUserByCompany(@PathVariable String groupInternalId) {
    return ResponseEntity.ok(
        service
            .usersByCompanyAndRole(groupInternalId, RoleEnum.REPRESENTANTELEGAL.getValue())
            .stream()
            .findFirst()
            .get());
  }

  @PutMapping(path = "/login")
  public ResponseEntity<UserDTO> loginCM(@RequestBody LoginDTO loginDto) {
    User user = this.service.loginCM(loginDto.getUsername(), loginDto.getPassword());
    return ResponseEntity.ok(UserMapper.buildUserDTO(user));
  }

  @PostMapping
  public ResponseEntity<UserDTO> create(@RequestBody UserDTO dto) {
    User user = this.service.create(UserMapper.buildUser(dto));
    User userDB = this.service.encodeNewUserPassword(dto.getMail());
    return ResponseEntity.ok(UserMapper.buildUserDTO(user));
  }

  @PutMapping
  public ResponseEntity<UserDTO> updateUserInformation(@RequestBody UserDTO dto) {
    try {
      return ResponseEntity.ok(this.service.updateUser(dto));
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PutMapping(path = "/block")
  public ResponseEntity<UserDTO> blockUser(@RequestBody User user) {
    User userDB = this.service.blockUser(user);
    return ResponseEntity.ok(UserMapper.buildUserDTO(user));
  }
}
